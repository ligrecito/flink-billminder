# Description

Quick POC for using Apache Flink for detecting bill payment frequency.


# Build

```
mvn clean install
```

# Use

Run apache flink:

```
$APACHE_FLINK_HOME/bin/start-local.sh
```

Submit flink job for execution

```
$APACHE_FLINK_HOME/bin/flink run $JOB_BUILD_JAR --input ./txn-dataset.csv
```



