package org.apache.flink.quickstart;

import org.apache.flink.api.common.functions.GroupReduceFunction;
import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.ExecutionEnvironment;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.api.java.utils.ParameterTool;
import org.apache.flink.util.Collector;
import org.apache.zookeeper.txn.Txn;

import java.time.Duration;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;


public class BillMinder {
    public static void main(String[] args) throws Exception {
        // parse parameters
        ParameterTool params = ParameterTool.fromArgs(args);
        String input = params.getRequired("input");

        // read external csv containing raw transactions
        final ExecutionEnvironment env = ExecutionEnvironment.getExecutionEnvironment();
        DataSet<TxnEvent> transactions = env.readCsvFile(input).pojoType(TxnEvent.class, "date", "cif", "billerCode");

        transactions
                .groupBy("cif", "billerCode")
                .reduceGroup(new BillFrequencyDetector())
                .print();

    }

    public static class BillFrequencyDetector implements GroupReduceFunction<TxnEvent, Tuple3<String, String, String>> {
        @Override
        public void reduce(Iterable<TxnEvent> eventsPerCifBiller, Collector<Tuple3<String, String, String>> collector) throws Exception {
            ArrayList<LocalDate> localDates = new ArrayList<>();

            Iterator<TxnEvent> it = eventsPerCifBiller.iterator();
            TxnEvent event = null;
            String cif = null;
            String billerCode = null;

            while (it.hasNext()){
                event = it.next();
                if (cif == null){
                    cif = event.getCif();
                    billerCode = event.getBillerCode();
                }
                localDates.add(LocalDate.parse(event.getDate(), DateTimeFormatter.ISO_LOCAL_DATE)); // parse all payment dates into LocalDates: yyyy-MM-dd
            }


            if (localDates.size() < 2) {
                collector.collect(new Tuple3<>(cif, billerCode, "__ONLY_ONE_BILL_FOUND__"));
            }else {

                // sort the arrayof dates
                Collections.sort(localDates);

                // a very dirty, innaccurate way to calculate bill frequency
                long freqDays = ChronoUnit.DAYS.between(localDates.get(0), localDates.get(1));

                collector.collect(new Tuple3<String, String, String>(cif, billerCode, String.valueOf(freqDays) + " days"));
            }

        }
    }

}
