package org.apache.flink.quickstart;

import java.time.Duration;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

/**
 * Created by javi on 15/07/2016.
 */
public class Test {
    public static void main(String[] args) {
        String date0 = new String("2016-01-01");
        String date1 = new String("2016-02-01");

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

        LocalDate ld0 = LocalDate.parse(date0, DateTimeFormatter.ISO_LOCAL_DATE);
        LocalDate ld1 = LocalDate.parse(date1, DateTimeFormatter.ISO_LOCAL_DATE);

        long days = ChronoUnit.DAYS.between(ld0, ld1);

        System.out.printf("duration = [ %d ] days", days);

    }
}
